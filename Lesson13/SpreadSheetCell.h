#pragma once

#include <string>
#include "SpreadSheetCellPosition.h"

class CellBase
{
public:
	CellBase(double dValue=0)
	{

	}
};

class SpreadSheetCell:public CellBase
{
public:
	SpreadSheetCell(std::initializer_list<double> args);
	SpreadSheetCell();

	SpreadSheetCell(double d);

	SpreadSheetCell(double d, SpreadSheetCellPosition p1);

	SpreadSheetCell(const SpreadSheetCell& other);
	//SpreadSheetCell(double initValue);
	~SpreadSheetCell()=delete;

	SpreadSheetCell& operator = (const SpreadSheetCell& other)=delete;
public:
	void SetValue(double inValue);
	double GetValue() const;

	std::string GetString() const;
	void SetString(std::string str);
private:
	double mValue = 0;
	SpreadSheetCellPosition mPosition1;
	SpreadSheetCellPosition mPosition2;
	//SpreadSheetCellPosition* pPosition;
	//SpreadSheetCellPosition& rPosition;
};



