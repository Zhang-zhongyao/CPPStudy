#include "pch.h"
#include "SpreadSheetCell.h"
#include <iostream>

SpreadSheetCell::SpreadSheetCell(std::initializer_list<double> args)
	:CellBase(0.0)
{
	for (const auto& arg:args)
	{


	}
	std::cout << "construct 1" << std::endl;
}

SpreadSheetCell::SpreadSheetCell()
	:CellBase(0),mValue(0.0)/*, pPosition(nullptr)*/
{
	mValue=0;
	std::cout << "construct 2" << std::endl;
}

SpreadSheetCell::SpreadSheetCell(double d)
{
	//pPosition = new SpreadSheetCellPosition();
	std::cout << "construct 3" << std::endl;
}

SpreadSheetCell::SpreadSheetCell(double d, SpreadSheetCellPosition p1)
	//:SpreadSheetCell(d)
{
	std::cout << "construct 4" << std::endl;
}

SpreadSheetCell::SpreadSheetCell(const SpreadSheetCell & other)
	:CellBase(0),mValue(other.mValue)
{
	//pPosition = new SpreadSheetCellPosition();
	//*pPosition = *other.pPosition;
	std::cout << "copy construct" << std::endl;
}

//SpreadSheetCell::SpreadSheetCell(double initValue)
//{
//	SetValue(initValue);
//}


//SpreadSheetCell::~SpreadSheetCell()
//{
//	//delete pPosition;
//	//pPosition = nullptr;
//	std::cout << "dtor called" << std::endl;
//}

//SpreadSheetCell & SpreadSheetCell::operator=(const SpreadSheetCell & other)
//{
//	// TODO: �ڴ˴����� return ���
//	if (this != &other)
//	{
//		this->mPosition1 = other.mPosition1;
//		this->mPosition2 = other.mPosition2;
//		this->mValue = other.mValue;
//	}
//
//	return *this;
//}

void SpreadSheetCell::SetValue(double inValue)
{
	this->mValue = inValue;
}

double SpreadSheetCell::GetValue() const
{
	return mValue;
}

void SpreadSheetCell::SetString(std::string str)
{
	try
	{
		double dValue = std::stod(str);
		SetValue(dValue);
	}
	catch(std::exception& e)
	{

	}
}

std::string SpreadSheetCell::GetString() const
{
	return std::to_string(mValue);
}
