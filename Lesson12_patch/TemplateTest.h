#pragma once
#include <vector>


template<class T,class T1,typename T2 >
class GamePiece
{
public:
	GamePiece(const T& id)
		:m_Id(id)
	{
	}

	void Draw()
	{
		//m_Device.draw(m_id);
	}

	std::string GetName()
	{
		return "";
	}

	inline void Test();

	virtual void Save();
public:
	T m_Id;
	T1 m_Device;
};


template<int SIZE_T>
class TestIntTemp
{

};


class GameBoard
{
public:
	std::vector<std::vector< GamePiece<int,int,int> > > m_GamePieces;

	template<typename T>
	void Draw()
	{

	}
};

template<class T, class T1, typename T2>
void GamePiece<T, T1, T2>::Test()
{

}

template<class T, class T1, typename T2>
inline void GamePiece<T, T1, T2>::Save()
{
}



template<typename ARG1, typename ARG2>
void TestTemplateFunc(ARG1 arg1,const ARG2& arg2)
{

}


//模板调用模板举例
template<typename T>
void TemplateFunc(T arg1)
{
	std::cout << "2" << std::endl;
}

void TemplateFunc(int arg1)
{
	//std::cout << "1"<<std::endl;
	TemplateFunc<int>(arg1);
}



template<typename T, typename T2>
void TemplateFunc(T arg1, T2 arg2)
{
	TemplateFunc<T>(arg1);
	TemplateFunc<T2>(arg2);
}

template<typename T, typename T2, typename T3>
void TemplateFunc(T arg1, T2 arg2, T3 arg3)
{

}

template<typename T, typename T2, typename T3, typename T4>
void TemplateFunc(T arg1, T2 arg2, T3 arg3, T4 arg4)
{

}
