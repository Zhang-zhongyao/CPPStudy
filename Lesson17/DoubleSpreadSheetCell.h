#pragma once
#include "SpreadSheetCell.h"

class DoubleSpreadSheetCell :
	public SpreadSheetCell
{
public:
	DoubleSpreadSheetCell();
	~DoubleSpreadSheetCell();

	//void SetValue(double dValue);
	//double GetValue();
	//SpreadSheetCell operator+ (const SpreadSheetCell& other);
	void SetValue(const std::string& strValue);
	void SetValue(double dValue);
	virtual std::string Trim(const std::string& strValue) override;
public:
	// ͨ�� SpreadSheetCell �̳�
	virtual std::string GetValue() const override;
	virtual std::string GetValue() override;
private:
	double mdValue;

};

