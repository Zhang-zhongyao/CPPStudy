﻿// Lesson02.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <array>
#include <vector>

enum class enBlackPiece
{
	Jiang=9,
	Shi,
	Xiang,
	Ma,
	//....

	Pao =10,
	Cu,
};

enum class enRedPiece
{
	Shuai=9,
	Bing=11,
};

struct MyStruct
{
	char Field1;

	char Field2;

};



bool cond1()
{
	std::cout << "cond1\n" ;
	return false;
}


bool cond2()
{
	std::cout << "cond2\n" ;
	return true;
}


auto add(int a, int b)
{
	return a + b;
}

union MyUnion
{

};

class MyClass
{

};


int main()
{
	//bool bRes = cond1() && cond2();

	int myArray[3] = {0};
	for (size_t i = 0; i < 3; i++)
	{
		myArray[i] = i;
	}

	int myArray2d[3][4];

	std::array<int, 3> arr;

	std::vector<int> vec;
	vec.push_back(1);
	
	std::cout << arr .size()<< std::endl;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
