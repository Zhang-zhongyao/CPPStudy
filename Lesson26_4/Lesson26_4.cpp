﻿// Lesson26_4.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "VariadicTemplate.h"
#include <tuple>

int main()
{
	std::string str("abc");
	processValues(1, std::move(str), 3.14f);
	//handle(1)
	//processValues("abc",3.14)
	//handle("abc");
	//processValues(3.14)
	//handle(3.14)
	//processValues()
	//-------------------------------
	//processValues();
	//可读可写
	MyInterface<ReadInterface, WriteInterface> ReadWriteInterface;

	ReadWriteInterface.Read();
	ReadWriteInterface.Write();

	//可读可打印
	MyInterface<ReadInterface, PrintInterface> ReadPrintInterface;
	ReadPrintInterface.Read();
	ReadPrintInterface.Print();


	printValues(1, "UWS", 2.535);//((std::cout<<1<<std::endl),(std::cout<<"UWS"<<std::endl),(std::cout<<2.535<<std::endl))

	std::cout<<"r="<<RightFolderSumOfValues(1,2,3,4,5,6,7,8,9,10)<<std::endl;
	std::cout << "l=" << LeftFolderSumOfValues(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) << std::endl;

	std::cout << Factorial<5>::value << std::endl;

	constexpr auto fac5 = factorial(5);

	std::cout << fac5 << std::endl;

	Loop<3>::Do(DoWork);

	std::tuple<int, std::string, double> isd_tuple = std::make_tuple(2,"ABC",3.14);

	std::cout << std::get<0>(isd_tuple) << std::endl;
	std::cout << std::get<1>(isd_tuple) << std::endl;
	std::cout << std::get<2>(isd_tuple) << std::endl;

	//tuple_print< std::tuple<int, std::string, double>,
	//	std::tuple_size<std::tuple<int, std::string, double>>::value> tuple_prnt(isd_tuple);
	tuple_printer(isd_tuple);
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
