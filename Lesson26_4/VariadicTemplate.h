#pragma once
#include <iostream>
#include <string>
#include <cstdint>

void handleValue(int arg)
{
	std::cout << "int:" << arg << std::endl;
}

void handleValue(const char* arg)
{
	std::cout << "const char*:" << arg << std::endl;
}

//void handleValue(std::string arg)
//{
//	std::cout << "std::string:" << arg << std::endl;
//}

void handleValue(std::string&& arg)
{
	std::cout << "r std::string:" << arg << std::endl;
}

void handleValue(double arg)
{
	std::cout <<"double:"<< arg << std::endl;
}

void processValues()
{

}

//template<typename T,typename ... ArgTypes>
//void processValues(T arg1,ArgTypes ... args)
//{
//	handleValue(arg1);
//	processValues(args ...);
//}

template<typename T1,typename ... ArgTypes>
void processValues(T1&& arg1, ArgTypes&& ... args)
{
	std::cout << "args:" << sizeof ... (args) << std::endl;
	handleValue(std::forward<T1>(arg1));
	processValues(std::forward<ArgTypes>(args) ...);
	//processValues(std::forward<Arg2Type>(args2),std::forward<Arg3Type>(args3),std::forward<Arg4Type>(args4),...);
}



class ReadInterface
{
public:
	void Read()
	{

	}
};

class WriteInterface
{
public:
	void Write()
	{

	}
};

class PrintInterface
{
public:
	void Print()
	{

	}
};

class ZipInterface
{
public:
	void Zip()
	{

	}
};

template<typename ... Mixins>
class MyInterface :public Mixins ...
{
public:
	//MyInterface(const Mixins& ... mixins) :Mixins(mixins) ...{}
};


//template<typename T1,typename ... ArgTypes>
//double Sum(T1 arg1,ArgTypes ... args)
//{
//	double sum = arg1;
//	sum +=Sum(args...);
//	return sum;
//}

//init + ... args -->init +arg1+arg2+arg3....

template<typename ... Tn>
void processValues(const Tn& ... args)
{
	(handleValue(args),...);//, ���� handleValue(arg1),handleValue(arg2),handleValue(arg3)...
}

template<typename ... Tn>
void printValues(const Tn& ... values)
{
	((std::cout<<values<<" "),...);
}

template<typename T,typename ... Values>
double RightFolderSumOfValues(const T& init, Values... values)
{
	return (values + ... + init);
}

template<typename T, typename ... Values>
double LeftFolderSumOfValues(const T& init, Values... values)
{
	return (init + ... + values);//init op ... op pack
}



template<unsigned int N>
class Factorial
{
public:
	static const uint64_t value = N * Factorial<N-1>::value;
};



template<>
class Factorial<0>
{
public:
	static const uint64_t value = 1;
};


constexpr uint64_t factorial(size_t N)
{
	if (N == 0)
	{
		return 1;
	}
	else
	{
		return N * factorial(N - 1);
	}
}


template<size_t N>
class Loop
{
public:
	template<typename FuncType>
	static inline void Do(FuncType func) 
	{
		Loop<N - 1>::Do(func);
		func(N);
	}
};

template<>
class Loop<0>
{
public:
	template<typename FuncType>
	static inline void Do(FuncType func)
	{
		
	}
};


inline void DoWork(size_t i)
{
	std::cout << "DoWork(" << i << ")" << std::endl;
}


template<typename TupleType,size_t N>
class tuple_print
{
public:
	tuple_print(const TupleType& t)
	{
		tuple_print<TupleType, N - 1> tp(t);
		std::cout << std::get<N - 1>(t) << std::endl;

	}
};


template<typename TupleType>
class tuple_print<TupleType, 1>
{
public:
	tuple_print(const TupleType& t)
	{
		std::cout << std::get<1 - 1>(t) << std::endl;
	}
};

template<typename TupleType>
class tuple_print<TupleType, 0>
{
public:
	tuple_print(const TupleType& t)
	{

	}
};

template<typename TupleType>
void tuple_printer(const TupleType& tp)
{
	tuple_print< TupleType,std::tuple_size<TupleType>::value> tuple_prnt(tp);
}