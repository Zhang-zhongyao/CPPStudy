#pragma once

template<typename T,/*const T DEFAULT=T()*/const T& DEFAULT >
class ArrayList
{
public:
	typedef T value_type;
	ArrayList(size_t i=30)
		:mLen(i)
	{
		if (mLen > 0)
		{
			mPInt = new T[mLen];
			for (size_t i = 0; i < mLen; i++)
			{
				mPInt[i] = DEFAULT;
			}
		}
		else
		{
			mPInt = nullptr;
		}
	}
	~ArrayList()
	{
		delete[] mPInt;
		mPInt = nullptr;
	}
	void resize(size_t nLen)
	{
		delete[] mPInt;
		//参数检查...
		mLen = nLen;
		mPInt = new T[nLen];
		for (size_t i = 0; i < mLen; i++)
		{
			mPInt[i] = DEFAULT;
		}
	}

	value_type& operator [](size_t i)
	{
		//参数检查...
		return mPInt[i];
	}
	size_t size()
	{
		return mLen;
	}
private:
	T* mPInt;
	size_t mLen;
};

