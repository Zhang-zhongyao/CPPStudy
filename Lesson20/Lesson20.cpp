﻿// Lesson20.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <stdexcept>
#include <limits>
#include "CoordException.h"

#include "GameBoard.h"

void WriteLog()
{
	throw std::runtime_error("can't open log file");
}


void func_try_catch_block(int x)
try
{
	if (x == 0)
	{
		throw std::invalid_argument("invalid argument is zero!");
	}
}
catch (std::exception& e)
{

}

void MakeGameBoard()
{
	try
	{
		GameBoard gb(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
		gb.at(10, 4);

	}
	//catch (size_t p)
	//{
	//	std::cout << "error size_t pos:"<<p << std::endl;
	//}
	catch (const std::bad_alloc& t)
	{
		//std::cout << "except:" << t.what() << std::endl;
		//std::cout << "error any pos:" << std::endl;
		//
		//if not open
			std::throw_with_nested(std::runtime_error("can't open log file"));
		//else
		//	writelog;
	}
}

int main()
{
	try
	{
		std::shared_ptr<int> p = std::make_shared<int>();
		MakeGameBoard();
	}
	catch (const std::runtime_error& e2)
	{
		std::cout << "except runtime:" << e2.what() << std::endl;

		const auto* pNestExcept = dynamic_cast<const std::nested_exception*>(&e2);
		if (pNestExcept)
		{
			try
			{
				pNestExcept->rethrow_nested();
			}
			catch (const std::bad_alloc& e)
			{
				std::cout << "except bad alloc:" << e.what() << std::endl;
				throw;
			}
		}
	}

	return 0;
 }
 
// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
