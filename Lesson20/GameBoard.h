#pragma once

class GamePiece;
class GameBoard
{
public:
	GameBoard(size_t width,size_t height);

	GameBoard(const GameBoard& src);

	GameBoard& operator = (const GameBoard& src);

	virtual ~GameBoard();

	GamePiece& at(size_t x, size_t y);

	const GamePiece& at(size_t x, size_t y) const;

	size_t getWidth() const;

	size_t getHeight() const;

	friend void swap(GameBoard& first, GameBoard& second) noexcept;

private:
	void verifyCoord(size_t x, size_t y) const;

	void cleanup() noexcept;
private:
	GamePiece** mCells = nullptr;
	size_t mWidth = 0;
	size_t mHeight = 0;
};

