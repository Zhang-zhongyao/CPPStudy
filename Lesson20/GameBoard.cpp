#include "pch.h"
#include "GameBoard.h"
#include "GamePiece.h"
#include "CoordException.h"
#include <utility>

GameBoard::GameBoard(size_t width, size_t height)
{
	mCells = new GamePiece*[mWidth] {};//set array element to null
	mWidth = width; 
	mHeight = height;
	try 
	{
		for (size_t i = 0; i < mWidth; i++)
		{
			mCells[i] = new GamePiece[mHeight];
		}
	}
	catch (...)
	{
		//cleanup();
		std::throw_with_nested(std::bad_alloc());
	}
}

GameBoard::GameBoard(const GameBoard & src)
	:mWidth(src.mWidth),mHeight(src.mHeight)
{
	mCells = new GamePiece*[mWidth];
	for (size_t i = 0; i < mWidth; i++)
	{
		mCells[i] = new GamePiece[mHeight];
		for (size_t j = 0; j < mHeight; j++)
		{
			mCells[i][j] = src.mCells[i][j];
		}
	}
}

GameBoard & GameBoard::operator=(const GameBoard & src)
{
	if (this == &src)
	{
		return *this;
	}
	GameBoard temp(src);
	swap(*this,temp);
	return *this;
}

GameBoard::~GameBoard()
{

}

GamePiece & GameBoard::at(size_t x, size_t y)
{
	return const_cast<GamePiece&>(std::as_const(*this).at(x,y));
}

const GamePiece & GameBoard::at(size_t x, size_t y) const
{
	// TODO: �ڴ˴����� return ���
	verifyCoord(x, y);
	return mCells[x][y];
}

size_t GameBoard::getWidth() const
{
	return mWidth;
}

size_t GameBoard::getHeight() const
{
	return mHeight;
}

void GameBoard::verifyCoord(size_t x, size_t y) const
{
	if (x > mWidth || x < 0)
	{
		throw new CoordException(x,y);
	}

	if (y > mHeight || y < 0)
	{
		throw new CoordException(x, y);
	}
}

void GameBoard::cleanup() noexcept
{
	for (size_t i = 0; i < mWidth; i++)
	{
		delete[] mCells[i];
	}
	delete[] mCells;
	mCells = nullptr;
	mWidth = 0;
	mHeight = 0;
}

void swap(GameBoard & first, GameBoard & second) noexcept
{
	std::swap(first.mWidth, second.mWidth);
	std::swap(first.mHeight, second.mHeight);
	std::swap(first.mCells, second.mCells);
}
