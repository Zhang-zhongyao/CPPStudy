﻿// Lesson11.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <memory>


class MySimpleClass:public std::enable_shared_from_this<MySimpleClass>
{
public:
	int CompareOtherPointer(std::shared_ptr< MySimpleClass> other)
	{
		auto self = shared_from_this();
		//auto self = std::shared_ptr<MySimpleClass>(this);//X
		return self->m_MyMember - other->m_MyMember;
	}

private:
	int m_MyMember;
};

void userResoure(std::weak_ptr<int> pWeakInt)
{
	std::shared_ptr<int> pShareInt = pWeakInt.lock();
	if (pShareInt)
	{
		std::cout << "not null" << std::endl;
	}
	else
	{
		std::cout << "is null" << std::endl;
	}
}

int main()
{
	//auto pInt=std::make_unique<int>(3);

	//auto pInt2(pInt.release());

	//if (pInt == nullptr)
	//{
	//	std::cout << "is null:"<<*pInt2 << std::endl;
	//}
	//else
	//{
	//	std::cout << "is not null:"<<*pInt2 << std::endl;
	//}
	//移动语义
	//{
	//	auto pInt3(std::move(pInt2));
	//	*pInt2 = 2;
	//	*pInt3 = 3;
	//	if (pInt2 == nullptr)
	//	{
	//		std::cout << "is null:" << *pInt2 << "," << *pInt3 << std::endl;
	//	}
	//	else
	//	{
	//		std::cout << "is not null:" << *pInt2 << "," << *pInt3 << std::endl;
	//	}
	//}

	//unique支持数组
	//auto myArr = std::make_unique<int[]>(20);

	//自定义删除器
	//int* pInt = (int*)malloc(sizeof(int));

	//free(pInt);

	//std::unique_ptr<int, decltype(free)*> myUniquePtr (pInt, free);
	
	//std::shared_ptr<int> pShareInt;

	//pShareInt.use_count();

	std::shared_ptr<int> pShareInt = std::make_shared<int>();
	std::weak_ptr<int> weakInt1(pShareInt);

	std::weak_ptr<int> weakInt2(pShareInt);
	userResoure(weakInt1);

	pShareInt.reset();

	userResoure(weakInt2);
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
