#include <iostream>
#include <array>
#include <initializer_list>
using namespace std;



struct point
{
    int x;
    int y;
};

auto getp()
{
    point p;
    p.x = 1;
    p.y = 2;
    return p;
}

int makeSum(initializer_list<int> lst)
{
    int sum=0;
    for (int v:lst)
    {
        sum += v;
    }
    return sum;
}

void func(int* p)
{
    if(p!=nullptr)
    {
        *p=3;
    }
}

int main()
{
    int* pInt = new int[4];

    //*pInt=9;
    *(pInt+0)=1;
    *(pInt+1)=2;

    pInt[2]=3;

    int sIntArr[4];

    sIntArr[0]=4;

    delete[] pInt;

    int x=0;

    int* pX=&x;

    *pX=4;

    func(pX);

    return 0;
}

