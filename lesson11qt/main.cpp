#include <iostream>
#include <memory>
using namespace std;

int* malloc_int(int init_val)
{
    int* p=(int*)malloc(sizeof(int));
    *p=init_val;
    return p;
}

void free_int(int* p)
{
    std::cout<<"free p"<<std::endl;
    free(p);
}

typedef void(*p_fun_deleter)(int*);

//typedef free_int* p_fun_deleter;

int main()
{
    std::unique_ptr<int,p_fun_deleter> pSmartInt(malloc_int(3),free_int);

    return 0;
}
