#pragma once
#include <string>

class Employee
{
public:
	Employee();
	~Employee();

	void SetSalary(int nSalary);

	int GetID() const;
public:
	std::string mName;

private:
	int mSalary;
	int mID;
};

