#pragma once

template<class T,typename argT2,typename argT3=int>
class GamePiece
{
public:
	void Draw();
public:
	T m_Id;
};

template<typename T>
void TemplateFunc(T arg1)
{

}

template<typename T,typename T2>
void TemplateFunc(T arg1,T2 arg2)
{
	TemplateFunc(arg1);
	TemplateFunc(arg2);
}

template<typename T,typename T2,typename T3>
void TemplateFunc(T arg1,T2 arg2,T3 arg3)
{

}

template<typename T, typename T2, typename T3, typename T4>
void TemplateFunc(T arg1, T2 arg2,T3 arg3,T4 arg4)
{

}

