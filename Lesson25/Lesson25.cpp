﻿// Lesson25.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <thread>
#include <chrono>
using namespace std::chrono;
#include <exception>
#include <atomic>

int id;
thread_local int len;

void counter(int id,int length)
{
	len = 0;
	for (size_t i = 0; i < length; i++)
	{
		std::cout << id << " run counter:"<< len << std::endl;
		++len;
	}
}


class Counter
{
public:
	Counter(int i, int len)
		:mId(i),mLength(len)
	{

	}
	void operator()() const
	{
		counter(mId,mLength);
	}

	void process() const
	{
		counter(mId, mLength);
	}

private:
	int mId;
	int mLength;
};


void doSomeWork()
{
	for (size_t i = 0; i < 5; i++)
	{
		std::cout << "i=" << i << std::endl;
	}
	throw std::runtime_error("runtime exception from thread doSomeWork");
}

void threadFun(std::exception_ptr& err)
{
	try 
	{
		doSomeWork();
	}
	catch (...)
	{
		std::cout << "catch an exception from doSomeWork" << std::endl;
		err = std::current_exception();
	}
}

class Foo
{
private:
	int mArray[123];
};

class Bar 
{
private:
	int mInt;
};

void increment(std::atomic<int>& counter)
{
	int result = 0;
	for (size_t i = 0; i < 100; ++i)
	{
		++result;
		std::this_thread::sleep_for(1ms);
	}
	counter += result;
}


int main()
{
	//std::thread t1(counter,1,30);
	//std::thread t2(counter, 2, 40);

	//std::thread t3( Counter(3,90) );
	//std::thread t4{ Counter{4,60} };

	//int len = 29;
	//int id = 5;
	//std::thread t5([id,len]() {
	//	counter(id, len);
	//});

	//Counter count6(6,18);
	//std::thread t6(&Counter::process, &count6);

	////std::bind+function

 //   std::cout << "Hello World!\n"; 
	//
	//t2.join();
	//t1.join();
	//t3.join();
	//t4.join();
	//t5.join();
	//t6.join();

	try
	{
		std::exception_ptr err;
		std::thread t(threadFun,std::ref(err));

		t.join();
		if (err)
		{
			std::rethrow_exception(err);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "exception happened:"<<e.what()<<std::endl;
	}



	std::atomic_bool bValid = false;//std::atomic<bool>
	std::atomic_int iValue = 19;

	iValue.is_lock_free();

	//std::atomic<std::tuple<int, std::string, double> > isd_value;


	std::atomic<Foo> f;
	std::cout << std::is_trivially_copyable_v<Foo> << " " << f.is_lock_free() << std::endl;
	
	std::atomic<Bar> b;
	std::cout << std::is_trivially_copyable_v<Bar> << " " << b.is_lock_free() << std::endl;


	std::atomic<int> counter = 0;
	std::vector<std::thread> threads;
	for (size_t i = 0; i < 10; i++)
	{
		threads.push_back(std::thread{ increment,std::ref(counter) });
	}

	for (size_t i = 0; i < threads.size(); i++)
	{
		threads[i].join();
	}
	std::cout << "Last Counter result=" << counter << std::endl;

	//std::atomic<T>::compare_exchange_strong(T& expected,T desired)
	//{
	//	if (*this == expected)
	//	{
	//		*this = desired;
	//		return true;
	//	}
	//	else
	//	{
	//		expected = *this;
	//		return false;
	//	}
	//}
	//boost::lockfree::xxx
}

//DWORD ThreadProc(LPVOID pParam);

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
