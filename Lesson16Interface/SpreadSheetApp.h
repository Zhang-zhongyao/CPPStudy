#pragma once

#include <string>
#include <memory>
#include "Lesson16Interface.h"


class SPREADSHEET_API SpreadSheetApp
{
public:
    SpreadSheetApp();
	SpreadSheetApp(const SpreadSheetApp& rhs);
	SpreadSheetApp& operator = (const SpreadSheetApp& rhs);
	~SpreadSheetApp();
public:
	std::string GetDefaultSavePath() const;

	friend void swap(SpreadSheetApp&first, SpreadSheetApp& second) noexcept;
private:
	class Impl;
	std::unique_ptr<Impl> mImpl;
};
