#include "stdafx.h"
#include "SpreadSheetAppImpl.h"


SpreadSheetApp::Impl::Impl()
	:mPtr(nullptr)
{
}

SpreadSheetApp::Impl::Impl(const Impl & rhs)
	:mPtr(nullptr)
{
	isCreated = rhs.isCreated;
	if (rhs.mPtr)
	{
		mPtr = new int(*rhs.mPtr);
	}
}

SpreadSheetApp::Impl & SpreadSheetApp::Impl::operator=(const Impl & rhs)
{
	if (this == &rhs)
	{
		return *this;
	}
	Impl temp(rhs);
	swap(temp);
	return *this;
}

std::string SpreadSheetApp::Impl::GetDefaultSavePath() const
{
	if (IsMobile())
	{
		return "\\sdcard\\mnt\\Doc";
	}
	return "C:\\Documents\\";
}

bool SpreadSheetApp::Impl::IsMobile() const
{
	return true;
}

void SpreadSheetApp::Impl::swap(Impl & first) noexcept
{
	std::swap(this->isCreated,first.isCreated);
	std::swap(this->mPtr, first.mPtr);
}

