#include "stdafx.h"
#include "SpreadSheetApp.h"
#include "SpreadSheetAppImpl.h"

SpreadSheetApp::SpreadSheetApp()
{
	mImpl = std::make_unique<SpreadSheetApp::Impl>();
}

SpreadSheetApp::SpreadSheetApp(const SpreadSheetApp & rhs)
{
	mImpl = std::make_unique<SpreadSheetApp::Impl>(rhs.mImpl);
}

SpreadSheetApp & SpreadSheetApp::operator=(const SpreadSheetApp & rhs)
{
	// TODO: �ڴ˴����� return ���
	*mImpl = *rhs.mImpl;
	return *this;
}

SpreadSheetApp::~SpreadSheetApp()
{
}

std::string SpreadSheetApp::GetDefaultSavePath() const
{
	return mImpl->GetDefaultSavePath();
}

void swap(SpreadSheetApp & first, SpreadSheetApp & second) noexcept
{
	std::swap(first.mImpl, second.mImpl);
}
