#pragma once

#ifdef LESSON16INTERFACE_EXPORTS

#define SPREADSHEET_API __declspec(dllexport)

#else

#define SPREADSHEET_API __declspec(dllimport)

#endif // 
