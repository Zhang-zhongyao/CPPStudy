# CPPStudy

#### 介绍
C++编程学习课程 是以《C++高级编程》 第4版(《Professional C++,4th Edition》)为教材蓝本，讲述C++编程语言(特别是C++11/14/17)的各个方面

#### 主题内容
- C++ 入门介绍
- 编码风格约定
- C++面向对象程序设计
- 指针和内存管理
- 深入类与对象
- C++各种灵活的关键字
- C++11 特性与右值介绍
- 模板类和模板函数
- 异常处理
- C++标准库，算法，容器和迭代器
- 新标准工具库
- 多线程编程
- 高级模板
- 设计模式(选讲)
- 各个主题之间会穿插介绍C++17的一些内容


#### 章节内容
1. C++ 入门基础知识，学习环境和第一个Hello,World程序
2. 自定义类型，if,switch 新语法，数组介绍
3. 结构化绑定，循环，堆栈、指针，动态内存分配
4. 指针回顾和拓展
5. 类、OOP、统一初始化
6. 第一个完整的C++程序例子
7. 字符串
8. 编码风格
9. 程序设计的重要性
10. 内存和指针
11. 智能指针
12. 模板类和模板函数
13. 熟悉类和对象（多个小节）
14. 类和对象深入
15. static ，const，mutable 在成员函数，成员变量，重载中的使用，=delete，=default 语法介绍
- 15.2. inline变量，嵌套类，操作符重载介绍
16. 操作符重载介绍
17. pImpl模式，虚函数虚表
- 17.2.  继承中复用父类的代码和多继承
- 17.3.  继承中更多的细节问题
18. C++ 特性复习
- 18.3. const，constexpr，类型别名，[[特性]]
19. C语言中的输入输出和C++输入输出流介绍
- 19.2. 输入流、字符串流、文件流
20. C++异常处理介绍
- 20.2. 嵌套异常、异常的重新抛出、函数块异常
21. 操作符重载
- 21.2. operator *,operator->,operator new,operator delete 操作符的重载
22. 标准库概述
- 22.2. vector容器
- 22.3. vector容器的添加和删除元素
- 22.4. vector示例讲解和其他线性容器
- 22.5. 容器适配器和有序关联容器
- 22.6. map和multimap，无序关联容器，hash，其他容器
23. 算法介绍：find，find_if,accumulate 等算法
- 23.2. bind+function 和lambda表达式
- 23.3. 函数对象functor和invoke
- 23.4. 标准库常用算法介绍
24. raito(有理数)和chrono 时间处理库 介绍
- 24.2. optional，variant,any,tuple，filesystem 工具库介绍
25. 多线程介绍
- 25.2 跨线程的异常处理和原子变量
- 25.3 互斥量和锁
- 25.4 锁的应用，条件变量
- 25.5 future/promise 使用和多线程编程的最佳实践
26. 高级模板编程
- 26.1 深入了解模板参数
- 26.2 模板部分特例化
- 26.3 模板递归
- 26.4 可变参数模板和折叠表达式
- 26.5 模板元编程介绍
- 26.6 constexpr if/trait/enable_if
#### 使用说明

1. 没有讲字符串的本地化
2. 没有讲正则表达式
3. 设计模式为选讲课程

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
5. 视频课程链接:https://h5.m.taobao.com/awp/core/detail.htm?id=607038843864&spm=a2141.7631671.content.2

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)