#pragma once
#define NOT_FOUND_INDEX  ((size_t)-1)
template<typename T>
size_t Find(T const & value,T const * pArray, size_t nArrLen)
{
	std::cout << "generic" << std::endl;
	for (size_t i = 0; i < nArrLen; i++)
	{
		if (value == pArray[i])
		{
			return i;
		}
	}
	return NOT_FOUND_INDEX;
}


//template<typename T>
//size_t Find<T*>(T* const& value, T* const * pArray, size_t nArrLen)
//{
//	for (size_t i = 0; i < nArrLen; i++)
//	{
//		if (*value == *pArray[i])
//		{
//			return i;
//		}
//	}
//	return NOT_FOUND_INDEX;
//}

template<typename T>
size_t Find(T* const& value, T* const * pArray, size_t nArrLen)
{
	std::cout << "ptr" << std::endl;
	for (size_t i = 0; i < nArrLen; i++)
	{
		if (*value == *pArray[i])
		{
			return i;
		}
	}
	return NOT_FOUND_INDEX;
}

//template<>
//size_t Find(char* const& value, char* const * pArray, size_t nArrLen)
//{
//	for (size_t i = 0; i < nArrLen; i++)
//	{
//		if (_stricmp(value,pArray[i])==0)
//		{
//			return i;
//		}
//	}
//	return NOT_FOUND_INDEX;
//}

template<>
size_t Find<const char*>(const char* const& value,const char* const * pArray, size_t nArrLen)
{
	std::cout << "const char*" << std::endl;
	for (size_t i = 0; i < nArrLen; i++)
	{
		if (_stricmp(value, pArray[i]) == 0)
		{
			return i;
		}
	}
	return NOT_FOUND_INDEX;
}


//size_t Find(const char*& value, const char** pArray, size_t nArrLen)
//{
//	std::cout << "over load" << std::endl;
//	for (size_t i = 0; i < nArrLen; i++)
//	{
//		if (_stricmp(value, pArray[i]) == 0)
//		{
//			return i;
//		}
//	}
//	return NOT_FOUND_INDEX;
//}