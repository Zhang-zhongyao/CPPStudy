#pragma once
#include <string>
#include <iostream>

class Process
{
public:
	Process(std::string processName)
		:mProcessName(processName)
	{
	}
	~Process();

	std::string GetProcessName()
	{
		return mProcessName;
	}

	//进程执行任务
	void doWorkDuringTimeSlice()
	{
		std::cout << "Process:" << mProcessName << " is working ..." << std::endl;
	}

	bool operator == (const Process& rhs)
	{
		return mProcessName == rhs.mProcessName;
	}
private:
	std::string mProcessName;
};

