#pragma once
#include <vector>

template<typename T>
class RoundRobin
{
public:
	RoundRobin()
	{
		mCurrentIter = mElements.begin();
	}
	~RoundRobin()
	{

	}

public:
	void add(const T& element)
	{
		int pos = mCurrentIter - mElements.begin();
		mElements.push_back(element);
		mCurrentIter = mElements.begin() + pos;
	}
	bool del(const T& element)
	{
		bool bDeleted = false;
		for (auto it = mElements.begin();it!=mElements.end();++it)
		{
			if (*it == element)
			{
				//找到了要删除的元素;
				if (mCurrentIter < it)
				{
					int pos = mCurrentIter - mElements.begin();
					mElements.erase(it);
					mCurrentIter = mElements.begin() + pos;
				}
				else if (mCurrentIter == it)
				{
					if (mCurrentIter == mElements.end() - 1)
					{
						mCurrentIter = mElements.erase(it);
						mCurrentIter = mElements.begin();
					}
					else
					{
						mCurrentIter = mElements.erase(it);
					}
				}
				else if (mCurrentIter > it)
				{
					int pos = (mCurrentIter - mElements.begin()) - 1;
					mElements.erase(it);
					mCurrentIter = mElements.begin() + pos;
				}
				bDeleted = true;
				break;
			}
		}
		return bDeleted;
	}
	//用来获取下一个任务
	T& getNext()
	{
		if (mElements.empty())
		{
			throw std::out_of_range("no element to get next!");
		}
		T& ret = *mCurrentIter;
		++mCurrentIter;
		if (mCurrentIter == end(mElements))
		{
			mCurrentIter = begin(mElements);
		}
		return ret;
	}
private:
	std::vector<T> mElements;
	//int mCurrentElementIndex; //保存下标
	typename std::vector<T>::iterator mCurrentIter; //保存迭代器
};

