#pragma once

#include "RoundRobin.h"
#include "Process.h"

class Scheduler
{
public:
	Scheduler();
	~Scheduler();
	//ִ�е���
	void scheduleTimeSlice()
	{
		mProcesses.getNext().doWorkDuringTimeSlice();
	}

	void InsertProcess(const Process& process)
	{
		mProcesses.add(process);
	}

	void RemoveProcess(const Process& process)
	{
		mProcesses.del(process);
	}

private:
	RoundRobin<Process> mProcesses;
};

