﻿// Lesson26_5.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "TemplateMateCoding.h"
#include <type_traits>
#include <complex>
int main()
{
	auto tp = std::make_tuple(24,"abc",3.14);
	tuple_print(tp);

	//std::cout << std::is_integral<std::string>::value << std::endl;

	//my_integral_const<int, 2> int2;

	//std::cout << int2() << std::endl;

	//using is_integral_v = is_integral<std::string>::value;
	std::cout << std::is_integral_v<int> << std::endl;
	std::cout << std::is_class_v<std::string> << std::endl;


	process(3);
	process("abc");

	std::complex<double> cplx(2,3);
	process(cplx);

	check_type(1,2);
	check_type(22, 3.14);
	check_type(9.9, "this is char*");

	//Derived d;
	call_doit(123);

	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
