#pragma once

#include <string>
#include "SpreadSheet.h"



class SpreadSheetCell
{
public:
	SpreadSheetCell();

	SpreadSheetCell(const SpreadSheetCell& other) = default;
	SpreadSheetCell& operator = (const SpreadSheetCell& other) = default;

	~SpreadSheetCell();

	void SetValue(const std::string& strValue);
	std::string GetValue() const;
private:
	std::string mValue;

	//friend class SpreadSheet;
	//friend void SpreadSheet::SetCell(int, int, const std::string&);
	//friend std::string SpreadSheet::GetCell(int, int);

	friend void DumpCell(const SpreadSheetCell& other);
};

