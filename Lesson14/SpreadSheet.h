#pragma once
#include <string>
class SpreadSheetCell;

class SpreadSheet
{
public:
	
	SpreadSheet(int nRow,int nColumn);

	SpreadSheet(const SpreadSheet& other);

	SpreadSheet& operator=(const SpreadSheet& other);

	SpreadSheet(SpreadSheet&& other) noexcept;

	SpreadSheet& operator=(SpreadSheet&& other) noexcept;

	~SpreadSheet();
private:
	SpreadSheet();
public:
	/*
	 * @return 返回true表示设置值成功，否则失败，比如索引超出表格的行数或者列数
	*/
	bool SetCell(int nRow, int nColumn, const std::string& mValue);

	bool GetCell(int nRow,int nColumn,std::string& strOutValue) const;
public:
	friend void swap(SpreadSheet& first, SpreadSheet& second) noexcept;

	void Init();
	void Fini();
	void InitFrom(const SpreadSheet& other);
	void MoveFrom(SpreadSheet& other);
private:
	SpreadSheetCell** mCells;
	int mRow;
	int mColumn;
	std::string mName;
public:
	const static int kDefaultRow = 100;
	const static int kDefaultColumn = 26;
};


