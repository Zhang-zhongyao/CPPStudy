﻿// Lesson14.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <vector>
#include "SpreadSheet.h"

void printMsg(const std::string& strMsg)
{
	//std::cout <<"left:"<< strMsg << std::endl;

}

void printMsg(std::string&& strMsg)
{
	//std::cout << "right:" << strMsg << std::endl;
	//print(std::move(strMsg));
}

SpreadSheet CreateObject()
{
	return SpreadSheet(4, 5);
}

template<typename T>
void swap(T& a, T& b)
{
	T temp(std::move(a));
	a = std::move(b);
	b = std::move(temp);
}


int main()
{
	//SpreadSheet ss(4,5);
	std::vector<SpreadSheet> vecSpreadSheet;

	for (size_t i = 0; i < 2; i++)
	{
		std::cout << "i=" << i << std::endl;
		SpreadSheet ss(i + 1, 3);
		vecSpreadSheet.push_back(ss);
	}

	//移动赋值操作符演示效果

	//SpreadSheet s(2, 3);
	//s = CreateObject();

	//SpreadSheet s2(5, 6);

	//s2 = s;

	//copySS.SetCell(2, 3, "ABC");

	//std::string strValue;
	//copySS.GetCell(2, 3,strValue);
	//std::cout << strValue << std::endl;

	//copySS = ss;

	//printMsg("ABC");


	//std::string msg("Hello");
	//std::string msg2(" World!");
	//printMsg(msg);

	//int&& i = 2;

	//int a = 1, b = 2;
	//int&& j = (a + b);

	//int i = 0;
	//int x = 2 + ++i;

	//i = 0;
	//int y = 2 + i++;

	//std::cout << "x=" << x << ",y="<<y << std::endl;

	//ss.SetCell(100, 26, "ABC");

	//std::string strCellValue;

	//bool bGet = ss.GetCell(101, 26,strCellValue);

	//if (bGet)
	//{
	//	std::cout << "Cell Value is:"<< strCellValue << std::endl;
	//}
	//else
	//{
	//	std::cout << "Cell Not In Spread!" << std::endl;
	//}


	return 0;
}


// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
