#include "pch.h"
#include "SpreadSheetCell.h"


SpreadSheetCell::SpreadSheetCell()
{
}


SpreadSheetCell::~SpreadSheetCell()
{
}


void SpreadSheetCell::SetValue(const std::string& strValue)
{
	mValue = strValue;
}

std::string SpreadSheetCell::GetValue() const
{
	return mValue;
}
