﻿// Lesson19.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <stdio.h>

#include <sstream>
#include <fstream>

struct strudent
{
	int id;
	int age;
	char name[32];
	bool sex;//true,表示女生，false表示男生
	unsigned char height;//身高
};

void opStream(std::istream& inputStream)
{
	int a=0;
	a= inputStream.get();
}

int main()
{
	//printf("%d\n", 3);
	//printf("%s\n", "hello,world");
	//printf("0x%08X\n", 2019);
	//printf("%f\n", 3.14159265359);


	//FILE* fp = fopen("test.txt", "r");
	//if (!fp) {
	//	perror("File opening failed");
	//	return EXIT_FAILURE;
	//}

	//int c; // note: int, not char, required to handle EOF
	//while ((c = fgetc(fp)) != EOF) { // standard C I/O file reading loop
	//	putchar(c);
	//}
	//puts("");
	//if (ferror(fp))
	//	puts("I/O error when reading");
	//else if (feof(fp))
	//	puts("End of file reached successfully");

	//fclose(fp);

	//FILE* fp = fopen("test.bin", "wb");

	//strudent stus[] =
	//{
	//	{
	//		1,18,"Lucy",true,155
	//	},

	//	{
	//		1,17,"LiLei",false,160
	//	},

	//	{
	//		1,19,"Lili",true,158
	//	},
	//};
	//for (size_t i = 0; i < 3; i++)
	//{
	//	fwrite(stus + i, sizeof(strudent), 1, fp);
	//}
	//fclose(fp);

	//FILE* fp = fopen("test.bin", "rb");

	//strudent stu = {0};

	//while (!feof(fp))
	//{
	//	size_t bytes = fread(&stu, sizeof(stu), 1, fp);
	//	if (bytes <= 0)
	//	{
	//		break;
	//	}
	//	printf("id=%d,age=%d,name=%s,sex=%s,heigt=%d\n",stu.id,stu.age,stu.name,stu.sex?"girl":"boy",stu.height);
	//}

	////fclose(fp);

 //   //std::cout << "Hello World!\n"; 
	//std::cout << std::endl;

	//std::cout << "hello,world" << 2019 << 0xFF << std::endl;
	//const char* pStr = "Hello,I am here!\n";
	//std::cout.write(pStr,strlen(pStr));
	//std::cout.put('c');
	//std::cout.put('\n');
	//std::cout.flush();

	//bool bValue = true;
	//std::cout << std::boolalpha <<bValue << std::endl;

	//std::cout << std::hex << std::setw(8) << std::setfill('0') << 2019 << std::endl;

	//printf("%f\n", 3.14159265359);
	//std::cout << 3.1415926 << std::endl;

	//printf("input a int:");
	//int a;
	//scanf("%d",&a);

	//std::cout <<"a="<< a << std::endl;

	//std::cout << "input a string:";
	//std::string a;
	//
	//while (std::cin)
	//{
	//	int ch = std::cin.get();
	//	if (!std::cin || ch == std::char_traits<char>::eof())
	//	{
	//		break;
	//	}
	//	a += static_cast<char>(ch);
	//}

	//int number;
	//std::cin >> number;

	//if (std::cin.eof())
	//{
	//	std::cout << "cin is eof" << std::endl;
	//}
	//else
	//{
	//	std::cout << "cin is not eof" << std::endl;
	//}

	//std::string gestName;
	//int partySize = 0;
	//char ch;
	//std::cin >> std::noskipws;
	//while (std::cin >> ch)
	//{
	//	if (isdigit(ch))
	//	{
	//		std::cin.unget();
	//		if (std::cin.fail())
	//		{
	//			std::cout << "unget failed!" << std::endl;
	//		}
	//		break;
	//	}
	//	gestName += ch;
	//}

	//if (std::cin)
	//{
	//	std::cin >> partySize;
	//}
	//if (!std::cin)
	//{
	//	std::cerr << "Error getting party size" << std::endl;
	//	return 0;
	//}

	//std::cout << "Thanks " << gestName << ",party of " << partySize << std::endl;
	//if (partySize > 10)
	//{
	//	std::cout << "An extra gratuity will apply" << std::endl;
	//}


	//char ch1,ch2,ch3;
	//std::cin >> ch1;
	//std::cin.putback('e');
	//ch2 = std::cin.peek();

	//std::cin >> ch3;

	//std::cout << "ch1=" << ch1 << ",ch2=" << ch2 <<",ch3="<<ch3 << std::endl;

	//char buffer[4] = {0};

	//std::cin.getline(buffer,4);

	//std::cout << "buffer=" <<buffer<< std::endl;

	//std::string myStr;
	//std::getline(std::cin,myStr);

	//std::cout << "the input str is:"<< myStr << std::endl;

	//std::string str("FF");

	//std::istringstream iss(str);

	//int ff;
	//iss >>std::hex>> ff;

	//std::cout << ff << std::endl;

	//std::ostringstream oss;

	//int ff=0;
	//std::cin >> ff;

	//oss << std::hex<<ff;

	//std::cout << oss.str() << std::endl;

	//std::ofstream outFile("test.txt", std::ios_base::ate);
	//
	//if (!outFile.good())
	//{
	//	std::cout << "error happpend while open output file!" << std::endl;
	//	return -1;
	//}

	//outFile << "Hello,Current year is " << 2019 << std::endl;

	//for (size_t i = 0; i < 5; i++)
	//{
	//	outFile <<"line:" << i << std::endl;
	//}

	//outFile.close();
	std::ofstream os("test1.txt");
	std::ifstream is("test1.txt");
	std::string value("0");

	os << "Hello";
	is >> value;

	std::cout << "Result before tie(): \"" << value << "\"\n";
	is.clear();
	is.tie(&os);

	is >> value;

	std::cout << "Result after tie(): \"" << value << "\"\n";
	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
