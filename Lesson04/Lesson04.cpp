﻿// Lesson04.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include <memory>
#include <string>

class Employee
{
public:
	std::string GetName()
	{
		return "ABC";
	}
	int age;
};

void func(const Employee* x)
{
	//x->age = 12;
}

//void func(const Employee& x)
//{
//	x.age = 12;
//}

void func(Employee x)
{
	x.age = 12;
}

int main()
{
	int * pInt = new int;
	*pInt = 3;

	int* pIntArr = new int[3];
	pIntArr[0] = 1;
	*(pIntArr + 1) = 2;


	//std::unique_ptr<int> pUniInt(pInt);

	//std::unique_ptr<Employee> pEmployee = std::make_unique<Employee>();

	//std::unique_ptr<Employee[]> pEmpArr = std::make_unique<Employee[]>(3);


	//std::shared_ptr<int> pUniInt(pInt);

	//std::shared_ptr<Employee> pEmployee = std::make_shared<Employee>();

	//std::shared_ptr<Employee[]> pEmpArr(new Employee[3]);

	//std::cout << pEmployee->GetName() << std::endl;

	Employee e;

	auto x = e;

	auto s = {0};
	
	decltype(x) y;
	//e.age = 1;
	//func(e);
	//std::cout << e.age << std::endl;


}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
