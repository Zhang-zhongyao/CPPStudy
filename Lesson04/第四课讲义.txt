指针
new
new []

delete 
delete []

智能指针 

auto_ptr

C++11

unique_ptr
shared_ptr

	std::unique_ptr<int> pUniInt(pInt);

	std::unique_ptr<Employee> pEmployee = std::make_unique<Employee>();

	std::unique_ptr<Employee[]> pEmpArr = std::make_unique<Employee[]>(3);

	std::cout << pEmployee->GetName() << std::endl;


	//std::shared_ptr<int> pUniInt(pInt);

	//std::shared_ptr<Employee> pEmployee = std::make_shared<Employee>();

	std::shared_ptr<Employee[]> pEmpArr(new Employee[3]);

                //C++17 std::shared_ptr<Employee[]> pEmpArr = std::make_shared<Employee[]>(3);

const
定义常量
const int Xconst=1;

保护参数
void func(const Employee* x)

C++
引用
int x=12;
int& xRef=x;

异常：
try
{

}catch()
{

}

auto
C++11 自动类型推导

const & 类型限定符

decltype