#pragma once
#include "test.h"

class ConstRect
{
public:
	constexpr ConstRect(int width, int heigh)
		:m_width(width),m_heigh(heigh)
	{

	}
	//~ConstRect() = default;
private:
	int m_width;
	int m_heigh;
public:
	constexpr int getArea() const
	{
		return m_width * m_heigh;
	}
};

