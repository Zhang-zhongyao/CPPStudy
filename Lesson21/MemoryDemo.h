#pragma once
#include<new>
#include <stdexcept>

class MemoryDemo
{
public:
	MemoryDemo();
	virtual ~MemoryDemo() = default;

	void* operator new(size_t,const char* str,int x);
	void* operator new[](size_t);

	void* operator new(size_t, const std::nothrow_t&) noexcept;
	void* operator new[](size_t, const std::nothrow_t&) noexcept;


	void operator delete(void* ptr,size_t) noexcept;
	void operator delete[](void* ptr) noexcept;


	void operator delete(void* ptr, const std::nothrow_t&) noexcept;
	void operator delete[](void* ptr, const std::nothrow_t&) noexcept;

public:
	int x;
};

