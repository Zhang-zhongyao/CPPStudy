#include "pch.h"
#include "DoubleSpreadSheetCell.h"
#include <iostream>


DoubleSpreadSheetCell::DoubleSpreadSheetCell()
	:mdValue(0.0)
{
	m_Type = SpreadSheetCell::CellType::eCTDouble;
}

DoubleSpreadSheetCell::DoubleSpreadSheetCell(double dValue)
	:mdValue(dValue)
{
	m_Type = SpreadSheetCell::CellType::eCTDouble;
}


DoubleSpreadSheetCell::~DoubleSpreadSheetCell()
{
}


void DoubleSpreadSheetCell::SetValue(const std::string & strValue)
{
	mdValue = std::stod(strValue);
}

void DoubleSpreadSheetCell::SetValue(double dValue)
{
	mdValue = dValue;
}

std::string DoubleSpreadSheetCell::Trim(const std::string & strValue)
{
	std::string strTimmedValue = SpreadSheetCell::Trim(strValue);
	if (strTimmedValue.find(" ") != std::string::npos)
	{
		return "";
	}
	return strTimmedValue;
}

DoubleSpreadSheetCell DoubleSpreadSheetCell::operator-() const
{
	DoubleSpreadSheetCell cell;
	cell.SetValue(-mdValue);
	return cell;
}

DoubleSpreadSheetCell & DoubleSpreadSheetCell::operator++()
{
	// TODO: �ڴ˴����� return ���
	++mdValue;
	return *this;
}

DoubleSpreadSheetCell DoubleSpreadSheetCell::operator++(int n)
{
	DoubleSpreadSheetCell cell(*this);
	++(*this);
	return cell;
}

DoubleSpreadSheetCell::operator double() const
{
	return mdValue;
}

std::string DoubleSpreadSheetCell::GetValue() const
{
	return std::to_string(mdValue);
}

std::string DoubleSpreadSheetCell::GetValue()
{
	return std::to_string(mdValue);
}


//void DoubleSpreadSheetCell::SetValue(double dValue)
//{
//	mdValue = dValue;
//}
//
//double DoubleSpreadSheetCell::GetValue()
//{
//	return mdValue;
//}
