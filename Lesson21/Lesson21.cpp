﻿// Lesson21.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "DoubleSpreadSheetCell.h"
#include "SpreadSheet.h"
#include "Pointer.h"
#include "MemoryDemo.h"
#include <cstddef>

int main()
{
	DoubleSpreadSheetCell cell(3.14);
	//
	DoubleSpreadSheetCell dcell = cell++;

	++cell;

	DoubleSpreadSheetCellComparator comparator;

	int i = comparator(cell,dcell);

	//SpreadSheet ss(10,10);
	//SpreadSheetCell& cell = ss[Coord(3, 4)];
	//cell.SetValue("2019");

	//std::cout << i << std::endl;

	//Pointer<int> intPtr(new int);
	//*intPtr = 3008;

	//std::cout << *intPtr << std::endl;


	Pointer<DoubleSpreadSheetCell> cellPtr(new DoubleSpreadSheetCell);
	if (/*cellPtr != nullptr*//*cellPtr*//*cellPtr != NULL*//*!cellPtr*/false)
	{
		//(*cellPtr).SetValue("9012");
		//cellPtr->SetValue("9012");
		//error nullptr
	}

	//int ip = cellPtr;


	//DoubleSpreadSheetCell ddcell(3.14159);

	//DoubleSpreadSheetCell* pddcell=&ddcell;

	//void (DoubleSpreadSheetCell::*pSetValue)(const std::string &) = DoubleSpreadSheetCell::SetValue;

	//(pddcell->*pSetValue)("999");


	//double pi = static_cast<double>(ddcell);

	//double dx = static_cast<double>(ddcell) + 3.3;

	//std::cout << dx << std::endl;


	//void* pmemory = malloc(1024);

	//SpreadSheetCell* pCell = new(pmemory) SpreadSheetCell[20];

	MemoryDemo* mem = new("test new",2019) MemoryDemo();

	delete mem;

	mem = new MemoryDemo[10];

	delete[] mem;

	mem = new (std::nothrow) MemoryDemo();

	delete mem;

	mem = new (std::nothrow) MemoryDemo[10];

	delete[] mem;

	return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单

// 入门提示: 
//   1. 使用解决方案资源管理器窗口添加/管理文件
//   2. 使用团队资源管理器窗口连接到源代码管理
//   3. 使用输出窗口查看生成输出和其他消息
//   4. 使用错误列表窗口查看错误
//   5. 转到“项目”>“添加新项”以创建新的代码文件，或转到“项目”>“添加现有项”以将现有代码文件添加到项目
//   6. 将来，若要再次打开此项目，请转到“文件”>“打开”>“项目”并选择 .sln 文件
