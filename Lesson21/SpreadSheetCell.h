#pragma once

#include <string>
#include "SpreadSheet.h"
#include <string_view>


class SpreadSheetCell
{
public:
	SpreadSheetCell();

	SpreadSheetCell(const SpreadSheetCell& other) = default;
	SpreadSheetCell& operator = (const SpreadSheetCell& other) = default;

	virtual ~SpreadSheetCell();

	virtual void SetValue(const std::string& strValue);
    //void SetValue(float dValue)=delete ;

	virtual std::string GetValue() const;
	virtual std::string GetValue();

    virtual void ResetAccessCount(int x,int nAccessCount=0) final;

    inline int GetAccessCount() const;

    //SpreadSheetCell add(SpreadSheetCell& other);

    //SpreadSheetCell operator+ (const SpreadSheetCell& other);

	//SpreadSheetCell& operator += (const SpreadSheetCell& rhs);

	//bool operator == (const SpreadSheetCell& rhs);

	SpreadSheetCell operator -() const
	{
		SpreadSheetCell cell;
		std::string value = std::to_string(-std::stod(GetValue()));
		cell.SetValue(value);
		return cell;
	}

	//ǰ��++
	SpreadSheetCell& operator++()
	{
		return *this;
	}
	//����++
	SpreadSheetCell operator++(int n)
	{
		return SpreadSheetCell();
	}

	virtual std::string Trim(const std::string& strValue);
protected:
    /*mutable*/ int mAccessCount=0;

	//friend class SpreadSheet;
	//friend void SpreadSheet::SetCell(int, int, const std::string&);
	//friend std::string SpreadSheet::GetCell(int, int);

	friend void DumpCell(const SpreadSheetCell& other);

	friend std::ostream& operator <<(std::ostream& ostr,SpreadSheetCell& cell);
	//friend std::istream& operator >>(std::istream& ostr, SpreadSheetCell& cell);
public:
    enum class CellType:int
    {
        eCTNone=0,
        eCTInt=1,
        eCTDouble=2,
        eCTStr=3,
        eCTObj=4,
    };
public:
	CellType m_Type;
	std::string m_Value;
public:
	CellType GetType();
};


//SpreadSheetCell operator + (const SpreadSheetCell& lhs, const SpreadSheetCell& rhs);

